object FunctionalCollections extends App {

  def printDir(pathname: String): Unit ={
    val files = (new java.io.File(pathname)).listFiles
    val files2 = files.toList.filter(item => !item.toString.startsWith(raw""".\."""))
    println(files2)
  }
//  printDir(".")

  def twoEntries(pathname: String): Unit ={
    val files = (new java.io.File(pathname)).listFiles
    val onlyFiles = files.toList.filter(item => item.isFile)
    val directories = files.toList.filter(item => item.isDirectory)
    println(onlyFiles)
    println(directories)
  }
//  twoEntries(".")

  def pairObjects(pathname: String): Unit ={
    val files = (new java.io.File(pathname)).listFiles
    val pairFiles = files.toList.map(item => (item, item.length()))
    println(pairFiles)
  }
//  pairObjects(".")

  def tenSmallest(pathname: String): Unit ={
    val files = (new java.io.File(pathname)).listFiles
    val list = files.toList.map(item => (item.getName, item.length())).sortBy(_._2)
    list.slice(0,10).foreach(println(_))
    println()
  }
  tenSmallest("./src/main/scala")


  def mapSolution(pathname: String): Unit ={
    val files = (new java.io.File(pathname)).listFiles
  }
}
