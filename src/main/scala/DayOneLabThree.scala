case class Trade private (val id:String,
            val symbol:String,
            val quantity:Int,
            var price:Double) {

  def this(id: String,  symbol: String, quantity: Int){
    this(id, symbol, quantity, 0)
  }

  require(quantity > 0)
  def getPrice(): Double = price

  def setPrice(price: Double): Unit ={
    if(price > 0){
      this.price = price
    } else {
      this.price = 0.00
    }
  }

  def value(): Double = {
    this.quantity * this.price
  }
  override def toString = s"Trade({id = $id, symbol = $symbol, quantity = $quantity, price = $price})"
}

object Trade{
  def main(args: Array[String]): Unit = {
    val tradeInstance = Trade("001", "AAPL", 20, 155.00)
    tradeInstance.setPrice(10.00)
    println(tradeInstance)
    println()
    println(tradeInstance.value())
  }
}
