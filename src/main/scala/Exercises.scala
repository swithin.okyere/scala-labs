object Exercises {
 //Short hand
  val stringConcat = (a: String, b:String, c: String, d: String, e: String) => {
    val result = a + b + c + d + e
    result.length
  }
  // Function_X
  val stringConcatFX = new Function5[String, String, String, String, String, Int] {
    override def apply(a: String, b:String, c: String, d: String, e: String): Int = {
      val result = a + b + c + d + e
      result.length
    }
  }

  val functionReturn = (a:Int) => ((x:Int) => x * 2)



  def main(args: Array[String]): Unit = {
    println(stringConcat("hi", "hey", "hi", "hey", "hi"))
    println(stringConcatFX("hi", "hey", "hi", "hey", "hi"))
  }
}
