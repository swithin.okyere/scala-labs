import java.text.SimpleDateFormat
import scala.util.matching.Regex
object DayOneLabTwo{

  def main(args: Array[String]) {
    println(celToFahr(21))
    println(dateConverter("04/11/21"))
  }

  //Write a function that converts a celsius temperature to fahrenheit
  def celToFahr(number:Double): Double = {
    println(s"the number is ${number}")
    (number * 9 / 5) + 32
  }

  //Write a function that converts a String containing
  def dateConverter(myDate:String){
    val splitted = myDate.split("/")
    val day = splitted(0).toInt
    val month = splitted(1).toInt
    val year = splitted(2).toInt

    println(day)
    val dayGiven = day match {
      case 1|21|31 => "st"
      case 2|22 => "nd"
      case 3|23 => "rd"
      case _ => "th"
    }

    val monthGiven = month match {
      case 1 => "January"
      case 2 => "February"
      case 3 => "March"
      case 4 => "April"
      case 5 => "May"
      case 6 => "June"
      case 7 => "July"
      case 8 => "August"
      case 9 => "September"
      case 10 => "October"
      case 11 => "November"
      case 12 => "December"
    }

    println(s"$day$dayGiven $monthGiven 20$year")
  }

  //Using a regular expression, extract the fields from a line
  //root:x:0:0:root:/root:/bin/bash
  def regParser(string: String){
    val keyValuePattern: Regex = "([a-zA-Z]+):([a-zA-Z]+):([0-9]+):([0-9]+):([a-zA-Z]+):(/[a-zA-Z]+):([a-zA-Z]+):(/[a-zA-Z]+)".r
  }
}