def factorialBad(n:BigInt):BigInt = {
  if(n == 1) 1
  else n* factorialBad(n-1)
}

def factorial(n: BigInt): BigInt = {
  def factorialAcc(n: BigInt, acc: BigInt): BigInt = {
    if(n <=1) acc
    else factorialAcc(n-1, n*acc)
  }
  factorialAcc(n, 1)
}

def fibonacci(n: BigInt):BigInt={
  if(n <= 1) 1
  else fibonacci(n-1) + fibonacci(n-2)
}

def fibonacciTailRec(n: BigInt):BigInt = {
  def fibonacciAcc(n:BigInt, previous:BigInt, current: BigInt):BigInt = {
    if(n <= 1) current
    else fibonacciAcc(n-1, current, current + previous)
  }
  fibonacciAcc(n, 1, 1)
}