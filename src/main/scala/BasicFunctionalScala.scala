object BasicFunctionalScala extends App {
  def ops(operation: String, a: Int, b:Int): Int ={
    operation match {
      case "add" => a + b
      case "subtract" => a - b
      case "power" =>  scala.math.pow(a,b).toInt
      case _ => 1
    }
  }

  val ops2 = (operation2: String) => {
    operation2 match {
      case "add" => (x:Int, y:Int) => x + y
      case "subtract" => (x:Int, y:Int) => x - y
      case "power" => (x:Int, y:Int) => scala.math.pow(x,y).toInt
//      case _ => 1
    }
  }

  val add:Int = ops2("power")(2,3)
  println(add)
}