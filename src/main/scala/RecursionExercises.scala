object RecursionExercises {

  def sumBetweenTwoNumbers(a:Int, b:Int):Int = {
    def sumBetweenTwoNumbersAcc(a: Int, acc: Int):Int = {
      if (a == b) acc
      else sumBetweenTwoNumbersAcc(a+1, acc+a)
    }
    sumBetweenTwoNumbersAcc(a, 0)
  }

//  def stringConcat(n: Int, string: String): Unit ={
//    def strConcatAcc(n:Int, acc: String): Unit ={
//
//    }

//    strConcatAcc(n,)
//  }


  def main(args: Array[String]): Unit = {
    println("Hello")
    println(sumBetweenTwoNumbers(4,7))
  }
}
