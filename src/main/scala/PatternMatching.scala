class PatternMatching {

  final var JUNIOR_MEMBER_LIMIT = 5
  final var REGULAR_MEMBER_LIMIT = 10
  final var SENIOR_MEMBER_LIMIT = 15

  def canBorrow(membership_type: String, numOfBooks: Int): Boolean ={
    membership_type match {
      case ("junior") if numOfBooks > JUNIOR_MEMBER_LIMIT => false
      case ("regular") if numOfBooks > REGULAR_MEMBER_LIMIT => false
      case ("senior") if numOfBooks > SENIOR_MEMBER_LIMIT => false
      case _ => true
    }
  }
}

object PatternMatching {
  def main(args: Array[String]): Unit = {
    var pattern = new PatternMatching
    println(pattern.canBorrow("regular", 40))
  }
}