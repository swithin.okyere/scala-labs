abstract class Trade2(val id:String, var price:Double){

  def isExecutable()
//  override def toString = s"Trade({id = $id, symbol = $symbol, quantity = $quantity, price = $price})"
}
class EquityTrade(id: String, symbol: String, quantity: Int, price: Double) extends Trade2(id, price){
  override def isExecutable() = true
  override def toString = s"EquityTrade({id = $id, symbol = $symbol, quantity = $quantity, price = $price})"

  def value(): Double = {
    this.quantity * this.price
  }
}

class FxTrade(id: String, price: Double) extends Trade2(id, price){
  override def isExecutable() = false
  override def toString = s"FXTrade({id = $id, price = $price})"
}

trait FeePayable{
  final val FLAT_FEE = 10
}

trait Taxable{
  final val TAX_RATE = 12.5 / 100
}

class Transaction(id: String, symbol: String, quantity: Int, price: Double) extends EquityTrade(id, symbol, quantity, price) with Taxable with FeePayable {

  def amount():Double = {
    var tax = (value() + FLAT_FEE) * TAX_RATE
    value() + FLAT_FEE + tax
  }
}

object Transaction{
  def main(args: Array[String]): Unit = {
    var equityTrade = new Transaction("12", "AAPL", 10, 10)
    println(equityTrade.amount())
  }
}